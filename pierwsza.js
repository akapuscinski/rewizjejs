var apiPath = 'http://localhost:3000/';
var listName = '';

$(document).ready(function () {
    showMainPage();
    initIpList(); //fetch hosts' list
    $('.collapsible').collapsible({ //init collapsibles - required by materialize framework
        accordion: false
    });
    prepareTimeSaveListButton($('#time-save'), $('#time-ip-list'), $('#time-ip-select'));
    prepareTimeSaveListButton($('#transfer-save'), $('#transfer-ip-list'), $('#transfer-ip-select'));
    $('select').material_select(); //init selects - required by materialize framework
    $('.modal-trigger').leanModal({
        dismissible: true
    });


});

function initIpList() {
    $.getJSON(apiPath + "ips", function (data) {
        for (var i in data) {
            createIpListItem(data[i]);
        }
    });
}

function createIpListItem(ip) {
    item = document.createElement('li');
    header = document.createElement('div');
    body = document.createElement('div');
    list = document.createElement('ul');
    transfer = document.createElement('li');
    time = document.createElement('li');

    $(header).addClass('collapsible-header');
    $(body).addClass('collapsible-body');
    $(transfer).addClass('sub-menu');
    $(time).addClass('sub-menu');
    $(transfer).html('<i class="large material-icons">insert_chart</i>Transfer danych');
    $(time).html('<i class="large material-icons">schedule</i>Czas spÄdzony');
    $(header).html(ip);

    $(list).append($(transfer));
    $(list).append($(time));
    $(body).append($(list));

    $(item).append($(header));
    $(item).append($(body));
    $('#ip-list').append($(item));

    (function (q) {
        transfer.addEventListener('click', function () {
            showTransferPage(q)
        });
        time.addEventListener('click', function () {
            showTimePage(q)
        });
    })(ip); //need to use closure to prevent g variable from changing
}

function prepareTimeSaveListButton(button, input, name, select) {
    input.on('input', function (e) {
        if (input.val() == '') {
            button.addClass('disabled');
            button.removeClass('modal-trigger');
        } else {
            button.removeClass('disabled');
            button.addClass('modal-trigger');

        }

    });

    $('#list-name-ok').click(function (e) {
        var name = $('#list-name-input').val();
        if (name == '')
            return;

        var path = apiPath + 'lists/' + name + '/' + input.val().replace(new RegExp(' ', 'g'), ';');

        $.post(path, '',
            function (data, status) {

                Materialize.toast('Zapisano listÄ', 4000);
                getIpLists(select);

            }).fail(function () {
            alert("Lista o tej nazwie juĹź istnieje, zmieĹ nazwÄ");
        });

    });
}

function getIpLists(select) {
    $.getJSON(apiPath + 'lists', function (data) {
        console.log(data);

        if (data.length == 0)
            return;

        for (var i in data) {
            var option = $('<option></option>');
            option.attr('ip-list', data[i].ips);
            option.text(data[i].name);
            select.append(option);

            console.log(data[i]);
        }

        $('select').material_select();

    });
}


function showMainPage() {
    $('.page').hide();
    $('#main-page').show();
}

function showTransferPage(ip) {
    $('.page').hide();
    $('#transfer-page').show();
    modifyHeaderText('Transfer ' + ip);
    $('#transfer-get').show();
    $('#transfer-loader').hide();
    $('#transfer-chart').hide();
    $('#transfer-table').hide();
    prepareGetTransferButton(ip);
    getIpLists($('#transfer-ip-select'));
}

function showTimePage(ip) {
    $('.page').hide();
    $('#time-page').show();
    modifyHeaderText('SpÄdzony czas ' + ip);
    $('#spent-time-container').hide();
    $('#time-get').show();
    $('#time-loader').hide();
    $('#time-chart').hide();
    $('#time-table').hide();
    $('#time-ip-list-container').hide();
    prepareGetTimeButton(ip);
    prepareIpSelect();
    getIpLists($('#time-ip-select'));
}

function modifyHeaderText(text) {
    $('#header-text').text(text);
}

function prepareGetTransferButton(ip) {
    $('#transfer-get').click(function () {
        $(this).hide();
        showTransferLoader();

        var startDate = $('#transfer-start-date').val() == '' ? 'all' : $('#transfer-start-date').val();
        var endDate = $('#transfer-end-date').val() == '' ? 'all' : $('#transfer-end-date').val();
        var ipList = getIpListFromForms($('#transfer-ip-select option:selected'), $('#transfer-ip-list'), $('#transfer-get'));

        var path = 'transfer/' + ip + '/' + startDate + '/' + endDate + '/' + ipList;
        console.log(path);

        $.getJSON(apiPath + path, function (data) {
            console.log(data);
            if (data.downloads.length == 0 && data.uploads.length == 0) {
                alert("Brak wynikĂłw dla zadanych danych, zmieĹ dane");
                $('#transfer-get').show();
            } else
                drawTransferChart(data);
        });
    });
}

function showTransferLoader() {
    $('#transfer-loader').show();
    $('#transfer-get').hide();
    $('#transfer-chart').hide();
    $('#transfer-table').hide();
}

function showTransferData() {
    $('#transfer-loader').hide();
    $('#transfer-get').show();
    $('#transfer-chart').show();
    $('#transfer-table').show();
}

function showTimeLoader() {
    $('#time-loader').show();
    $('#time-chart').hide();
    $('#time-table').hide();
    $('#time-get').hide();
}

function showTimeChart() {
    $('#time-get').show();
    $('#time-loader').hide();
    $('#time-chart').show();
    $('#time-table').show();
}

function prepareIpSelect() {
    $('#time-ip-select').change(function () {
        $('#time-ip-list-container').hide();
        $("#time-ip-select option:selected").each(function () {
            if ($(this).val() == 'custom')
                $('#time-ip-list-container').show();
        });
    });
}

function prepareGetTimeButton(ip) {
    $('#time-get').click(function () {
        $(this).hide();

        var startDate = $('#time-start-date').val() == '' ? 'all' : $('#time-start-date').val();
        var endDate = $('#time-end-date').val() == '' ? 'all' : $('#time-end-date').val();
        var ipList = getIpListFromForms($('#time-ip-select option:selected'), $('#time-ip-list'), $('#time-get'));
        var path = 'timespent/' + ip + '/' + startDate + '/' + endDate + '/' + ipList;
        console.log(path);

        $.getJSON(apiPath + path, function (data) {
            console.log(data);
            if (data.times.length == 0) {
                alert("Brak wynikĂłw dla zadanych danych, zmieĹ dane");
                $('#time-get').show();
            } else
                drawTimeChart(data);
        });
    });
}
//$('#time-ip-select option:selected')
function getIpListFromForms(selectedOptions, customInput, updateButton){
    var ipList = '';
    var isCustomListSelected = false;
    selectedOptions.each(function () {
        if ($(this).val() == '')
            return true; //skip blank option, return works as continue in this jquery case
        if ($(this).val() == 'custom') {
            isCustomListSelected = true;
        } else {
            ipList += $(this).attr('ip-list');
            ipList += ';';
        }
    });

    if (isCustomListSelected)
        ipList += customInput.val().replace(new RegExp(' ', 'g'), ';');

    if (ipList.substr(ipList.length - 1, ipList.length) == ';')
        ipList = ipList.substr(0, ipList.length - 1); //remove last semicolon

    if(ipList.length<1)
        ipList = 'all';

    return ipList.replace(new RegExp('/', 'g'), 'm');
}

google.charts.load('current', {
    'packages': ['corechart']
});

function drawTransferChart(transfer) {
    var data = new google.visualization.DataTable();
    data.addColumn('datetime', 'Data');
    data.addColumn('number', 'Download [kB]');
    data.addColumn('number', 'Upload [kB]');

    var options = {
        vAxis: {
            minValue: 0,
        },
        hAxis: {
            format: "dd/MM HH:mm",
        },
        width: 900,
        height: 500,
        explorer: {
            actions: ['dragToZoom', 'rightClickToReset'],
            axis: 'horizontal',
            keepInBounds: true,
            maxZoomIn: 20.0
        }
    };

    var d = transfer.downloads;
    var u = transfer.uploads;

    //check if we have both series for download and upload
    var startD = d[0] != undefined ? d[0].date : -1;
    var endD = d[d.length - 1] != undefined ? d[d.length - 1].date : -1;
    var startU = u[0] != undefined ? u[0].date : -1;
    var endU = u[u.length - 1] != undefined ? u[u.length - 1].date : -1;

    //replace dates to millis
    var mstartD = startD != -1 ? Date.parse(startD) : startD;
    var mendD = endD != -1 ? Date.parse(endD) : endD;
    var mstartU = startU != -1 ? Date.parse(startU) : startU;
    var mendU = endU != -1 ? Date.parse(endU) : endU;

    //determine wchich date should be treated as start date and end date
    var start = mstartD < mstartU && mstartD != -1 ? mstartD : mstartU;
    var end = mendD > mendU ? mendD : mendU;
    var period = 24 * 60 * 60 * 1000; //each 30 minutes we count transfered data, may be changed to day etc
    var temp = (end - start) / period;
    end = temp % 1 == 0 ? end : start + (temp + 1) * period;

    for (var i = start; i <= end; i += period) {
        var tempD = 0;
        for (var k in d) {
            var millis = Date.parse(d[k].date);
            if (millis >= i && millis < i + period)
                tempD += d[k].download;
        }
        var tempU = 0;
        for (var j in u) {
            var millis = Date.parse(u[j].date);
            if (millis >= i && millis < i + period)
                tempU += u[j].upload;
        }
        data.addRows([[new Date(i), tempD, tempU]]);
        console.log(new Date(i) + " D: " + tempD + " U: " + tempU);
    }

    //            addTestTransferData(data);

    var chart = new google.visualization.LineChart(document.getElementById('transfer-chart'));
    chart.draw(data, options);

    showTransferData();
    addTransferRows(transfer);
}

function addTransferRows(data) {
    var d = data.downloadList;
    var u = data.uploadList;

    $('#transfer-download-body').empty();
    $('#transfer-upload-body').empty();

    for (var i in d) {
        var row = '<tr><td>' + d[i].ip + '</td><td>' + d[i].bytes / 1024 + '</td></tr>';
        $('#transfer-download-body').append(row);
    }

    for (var i in u) {
        var row = '<tr><td>' + u[i].ip + '</td><td>' + u[i].bytes / 1024 + '</td></tr>';
        $('#transfer-upload-body').append(row);
    }
}

function drawTimeChart(time) {
    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Data');
    data.addColumn('number', 'Czas [minuty]');

    var options = {
        vAxis: {
            minValue: 0,
        },
        width: 900,
        height: 500,
        explorer: {
            actions: ['dragToZoom', 'rightClickToReset'],
            axis: 'horizontal',
            keepInBounds: true,
            maxZoomIn: 4.0
        }
    };

    var times = time.times;

    //get start day
    var split = times[0].date.split(' ');
    var startMillis = Date.parse(split[0]);
    var period = 24 * 60 * 60 * 1000; //we counting time spent for each day, can be changed to hours etc
    //get end day
    split = times[times.length - 1].date.split(' ');
    var endMillis = Date.parse(split[0]) + 24 * 60 * 60 * 1000; //add millis for the whole day

    //add rows
    for (var i = startMillis; i <= endMillis; i += period) {
        var spent = 0;
        for (var j in times) {
            var date = times[j].date.split(' ');
            var millis = Date.parse(date);
            if (millis >= i && millis < i + period)
                spent += parseFloat(times[j].time);
        }
        data.addRows([[new Date(i), spent]]);
    }

    showTimeChart();

    //            addTestTimeData(data);

    var chart = new google.visualization.LineChart(document.getElementById('time-chart'));
    chart.draw(data, options);
}

function addTestTransferData(data) {
    for (var i = 0; i < 14; i++) {
        data.addRows([[new Date(2016, 05, i), Math.random() * 100, Math.random() * 100]]);
    }
}

function addTestTimeData(data) {
    for (var i = 0; i < 14; i++) {
        data.addRows([[new Date(2016, 05, i), Math.random() * 10]]);
    }
}


$(document).ready(function(){
  var fileInput = document.getElementById('file-input-1');
  fileInput.addEventListener('change', function (e) {
    var file = fileInput.files[0];
    var textType = /text.*/;

    var reader = new FileReader();

    reader.onload = function (e) {
      console.log(reader.result);
      getMethods(reader.result);

      //do something with result to test it
    }

    reader.readAsText(file);

  });
});

/*
Returns methods from snippet2 with their state compared to snippet2
*/
function getDiffMethods(snippet1, snippet2){

}

function DiffMethod(method, state){
  this.method = method; //see Method object
  this.state = state; //0 - same methods, -1 removed, 1 added, 2 changed - same name, different body or args
}

/*
Find all named methods without inner methods in javascript snippet
Returns
list of objects of Method type
*/
function getMethods(snippet){
    var varRegex = '/(var)(\s)(\S+)/g';
    var noVarRegex = '';

    var varResult = snippet.match('a');
    console.log(varResult);
    for(var i in varResult){
      console.log(varResult[i]);
    }
}

function Method(name, arg, body){
  this.name = name;
  this.args = args;
  this.body = body;
}

/*
Returns variables from snippet2 with their state compared to snippet2
*/
function getDiffVars(snippet1, snippet2){

}

function DiffVar(name, state){
  this.name = name;
  this.state = state; //0 - present in both snippets, -1 removed, 1 added
}

/*
Return list of variables from snippet, only global variables.
Note, variable is not only word preceded  with 'var'
Returns
list of strings - variable names;
*/
function getVars(snippet){

}

/*
Determine whether it's a reserved language keyword
list available here http://www.w3schools.com/js/js_reserved.asp
This function is necessary to find variables not declared with var
Returns
true if word is reserved false otherwise
*/
function isReserved(word){

}

/*
Compare two code snippets and find differences
*/
function getDiff(snippet1, snippet2){
  //use diff library
}

/*
Check if selected file is writen in javascript
Returns true/false
*/
function isWrittenInJS(snippet){
    //highlight.js offers a function to determine code language
}
